# fast-springboot

#### 介绍
springboot常用组件二次 封装，方便快速在项目中使用
spring boot常用组件独立模块整合，每个模块独立配置，通过配置类进行配置，实现由@ComponentScan()扫描即可自动配置。
项目结构：
fast-springboot
	springboot-web
	springboot-mybatisplus:多数据源，分库分表AbstractRoutingDataSource
	redis
	mongodb
	elasticsearch
	kafka
	RabbitMQ
	common：hutool等
	quartz：分布式定时任务
	pageHelper
	shiro：权限管理校验
	activiti：工作流
pom.xml：springboot依赖，其他不要在这里引入，根据模块引入

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
